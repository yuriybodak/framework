<h1>Posts</h1>
<a href="/post/add"><button>Create post</button></a>
<?php foreach($posts as $post) :?>
    <h2><a href="/post/get/<?=$post['id'];?>"><?=$post['title'];?></a></h2>
    <span>Published: <?=$post['created_at'];?></span><br>
    <span>Author: <a href="/user/edit/<?=$post['author'];?>"><?=$post['first_name'] . ' ' . $post['last_name'];?></a></span>
    <p><?=substr($post['text'], 0, 300) . '...';?></p>
    <span>Views: <?=$post['views'];?></span>
<?php endforeach;?>