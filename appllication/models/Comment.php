<?php

use Framework\Model;

class Comment extends Model{

    private $tableName = 'comment';

    public function getCommentsByPostId($postId)
    {
        $sql = "SELECT * FROM {$this->tableName} AS c INNER JOIN user AS u ON c.author_id = u.id  WHERE c.post_id=? ORDER BY c.created_at DESC";

        $query = $this->connect->prepare($sql);
        $query->execute([$postId]);

        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

}